let assistanceNumber = document.querySelector('.assistanceNumber div');

const targetNumber = 100; // Change this to your desired target number
let currentNumber = 0;

function updateCounter() {
  assistanceNumber.textContent = Math.round(currentNumber) + "+"; // Round the number for better display
}

function easeOutCubic(t) {
  return 1 - Math.pow(1 - t, 3);
}

function countToTarget() {
  const duration = 4000; // Total duration in milliseconds
  let startTime;
  let increment;

  function animate(timestamp) {
    if (!startTime) startTime = timestamp;
    const progress = (timestamp - startTime) / duration;

    if (progress < 1) {
      currentNumber = easeOutCubic(progress) * targetNumber;
      updateCounter();
      requestAnimationFrame(animate);
    } else {
      currentNumber = targetNumber; // Ensure the final count is exactly the target number
      updateCounter();
    }
  }

  requestAnimationFrame(animate);
}

document.getElementById("menu").addEventListener("click", function() {
  var nav = document.querySelector(".header ul");
  nav.classList.toggle("show");
});

// Trigger counting when the page loads (you can trigger it based on events)
countToTarget();

let buttonClosePopUp = document.querySelector('#closePopUp');
let images = document.querySelectorAll('.galerija-wrapper div img');
let popUpDiv = document.querySelector('.pop-up-image');
let imageToShow = document.querySelector('.pop-up-image img');
let overlay = document.querySelector('.overlay');

images.forEach((element) => {
  element.addEventListener("click", () => {
    imageToShow.setAttribute('src', element.getAttribute('src'));

    popUpDiv.style.display = 'block';
    overlay.style.display = 'block';
  })
})

buttonClosePopUp.addEventListener('click', () => {
  imageToShow.setAttribute('src', '');

  popUpDiv.style.display = 'none';
  overlay.style.display = 'none';
});

let aTags = document.querySelectorAll('.liMenu');
aTags.forEach((a) => {
  let sectionName = a.getAttribute('href');
  let section = document.querySelector(sectionName);
  a.addEventListener('click', (e) => {
    e.preventDefault();

    scrollToSection(section);
  });
})

function scrollToSection(section) {
  window.scrollTo({
    top: section.offsetTop,
    behavior: 'smooth'
  })
}



